#ifndef APPLICATIONLOGIC_H
#define APPLICATIONLOGIC_H

#include <QList>

#include <matrix.h>

class QTextStream;

class ApplicationLogic
{
public:
    ApplicationLogic(QTextStream &input, QTextStream &output);

    int addMatrix(const Matrix &mat);
    void removeMatrix(int id);
    int insertMatrix(int id, const Matrix &mat);

    const Matrix &getMatrix(int id) const;
    Matrix &getMatrix(int id);

    int getMatrixCount() const
    {
        return mMatrixList.size();
    }

private:
    QList<Matrix> mMatrixList;
    Matrix mTrash;
};

#endif // APPLICATIONLOGIC_H
