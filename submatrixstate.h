#ifndef SUBMATRIXSTATE_H
#define SUBMATRIXSTATE_H

#include <state.h>

class SubMatrixState : public State
{
public:
    SubMatrixState(State *parentState);

    const QString &getDescription() const
    {
        static const QString Desc = "Substract matrixes";
        return Desc;
    }

    State *process(QTextStream &input, QTextStream &output, ApplicationLogic *logic);
};

#endif // SUBMATRIXSTATE_H
