#ifndef ROOTSTATE_H
#define ROOTSTATE_H

#include <state.h>

class RootState : public State
{
public:
    RootState();

    virtual State *process(QTextStream &input, QTextStream &output,
                           ApplicationLogic *logic);

    const QString &getDescription() const
    {
        static const QString Desc = "Root";
        return Desc;
    }
};

#endif // ROOTSTATE_H
