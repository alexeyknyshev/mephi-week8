#include "rootstate.h"

#include <QTextStream>

RootState::RootState()
    : State(nullptr)
{
}

State *RootState::process(QTextStream &input, QTextStream &output,
                          ApplicationLogic *logic)
{
    return State::process(input, output, logic);
}
