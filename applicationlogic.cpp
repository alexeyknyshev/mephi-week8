#include "applicationlogic.h"

#include <rootstate.h>
#include <inputmatrixstate.h>
#include <loadmatrixstate.h>
#include <savematrixstate.h>
#include <showmatrixstate.h>
#include <editmatrixstate.h>
#include <addmatrixstate.h>
#include <submatrixstate.h>
#include <mulmatrixstate.h>
#include <exitstate.h>

ApplicationLogic::ApplicationLogic(QTextStream &input, QTextStream &output)
{
    RootState rootState;
    State *inputMatrixState = new InputMatrixState(&rootState, true);
    State *loadMatrixState = new LoadMatrixState(&rootState);
    State *saveMatrixState = new SaveMatrixState(&rootState);
    State *showMatrixState = new ShowMatrixState(&rootState);
    State *editMatrixState = new EditMatrixState(&rootState);
    State *addMatrixState = new AddMatrixState(&rootState);
    State *subMatrixState = new SubMatrixState(&rootState);
    State *mulMatrixState = new MulMatrixState(&rootState);
    State *exitState = new ExitState(&rootState);

    rootState += inputMatrixState;
    rootState += loadMatrixState;
    rootState += saveMatrixState;
    rootState += showMatrixState;
    rootState += editMatrixState;
    rootState += addMatrixState;
    rootState += subMatrixState;
    rootState += mulMatrixState;
    rootState += exitState;

    State *currentState = &rootState;

    while (currentState)
    {
        currentState = currentState->process(input, output, this);
    }
}

int ApplicationLogic::addMatrix(const Matrix &mat)
{
    return insertMatrix(mMatrixList.size(), mat);
}

void ApplicationLogic::removeMatrix(int id)
{
    mMatrixList.removeAt(id);
}

int ApplicationLogic::insertMatrix(int id, const Matrix &mat)
{
    id = qMax(0, qMin(id, mMatrixList.size()));
    mMatrixList.insert(id, mat);
    return id;
}

const Matrix &ApplicationLogic::getMatrix(int id) const
{
    if (id >= 0 && id < mMatrixList.size())
    {
        return mMatrixList.at(id);
    }

    return Matrix::null;
}

Matrix &ApplicationLogic::getMatrix(int id)
{
    if (id >= 0 && id < mMatrixList.size())
    {
        return mMatrixList[id];
    }

    mTrash.setNull();
    return mTrash;
}

