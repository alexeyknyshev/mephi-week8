#include "matrix.h"

#include <QStringList>

const Matrix Matrix::null;

Matrix::Matrix()
{ }

float Matrix::getAt(int row, int column) const
{
    if (row < mData.size())
    {
        if (column < mData.at(row).size())
        {
            return mData.at(row).at(column);
        }
    }

    return 0.0f;
}

void Matrix::setAt(int row, int column, float scalar)
{
    mData.reserve(row + 1);

    if (mData.size() < row + 1)
    {
        mData.resize(row + 1);
    }
    if (mData.at(row).size() < column + 1)
    {
        mData[row].resize(column + 1);
    }

    mData[row][column] = scalar;
}

Matrix Matrix::operator+(const Matrix &other) const
{
    if (!isOkForAddSubWith(other))
    {
        return Matrix::null;
    }

    Matrix result;
    for (int row = 0; row < getRowCount(); ++row)
    {
        for (int col = 0; col < getColumnCount(); ++col)
        {
            result.setAt(row, col, getAt(row, col) + other.getAt(row, col));
        }
    }

    return result;
}

Matrix Matrix::operator-(const Matrix &other) const
{
    if (!isOkForAddSubWith(other))
    {
        return Matrix::null;
    }

    Matrix result;
    for (int row = 0; row < getRowCount(); ++row)
    {
        for (int col = 0; col < getColumnCount(); ++col)
        {
            result.setAt(row, col, getAt(row, col) - other.getAt(row, col));
        }
    }

    return result;
}

Matrix Matrix::operator*(const Matrix &other) const
{
    Matrix result;

    const int resultRow = getRowCount();
    const int resultColumn = other.getColumnCount();

    if (!isOkForMulWith(other))
    {
        return result;
    }

    for (int row = 0; row < resultRow; ++row)
    {
        for (int col = 0; col < resultColumn; ++col)
        {
            float scalar = 0.0f;

            for (int i = 0; i < getColumnCount(); ++i)
            {
                scalar += getAt(row, i) * other.getAt(i, col);
            }

            result.setAt(row, col, scalar);
        }
    }

    return result;
}

bool Matrix::isOkForAddSubWith(const Matrix &other) const
{
    return (getRowCount()    == other.getRowCount() &&
            getColumnCount() == other.getColumnCount());
}

bool Matrix::isOkForMulWith(const Matrix &other) const
{
    return (getColumnCount() == other.getRowCount());
}

QString Matrix::toString() const
{
    QStringList tmp;
    tmp.reserve(mData.size());
    for (int i = 0; i < mData.size(); ++i)
    {
        tmp.append(QString());
    }

    bool anyCel = false;
    for (int row = 0; row < mData.size(); ++row)
    {
        tmp[row] += "| ";
        for (int col = 0; col < mData.at(row).size(); ++col)
        {
            tmp[row] += QString::number(mData.at(row).at(col));
            if (col < mData.at(row).size() - 1)
            {
                tmp[row] += "\t";
            }
            anyCel = true;
        }
        tmp[row] += " |";
    }

    if (!anyCel)
    {
        return "NULL";
    }

    return tmp.join("\n");
}
