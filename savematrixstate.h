#ifndef SAVEMATRIXSTATE_H
#define SAVEMATRIXSTATE_H

#include <state.h>

class SaveMatrixState : public State
{
public:
    SaveMatrixState(State *parentState);

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);

    const QString &getDescription() const
    {
        static const QString Desc = "Save matrix to file";
        return Desc;
    }
};

#endif // SAVEMATRIXSTATE_H
