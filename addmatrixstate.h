#ifndef ADDMATRIXSTATE_H
#define ADDMATRIXSTATE_H

#include <state.h>

class AddMatrixState : public State
{
public:
    AddMatrixState(State *parentState);

    const QString &getDescription() const
    {
        static const QString Desc = "Add matrixes";
        return Desc;
    }

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);
};

#endif // ADDMATRIXSTATE_H
