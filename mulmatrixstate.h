#ifndef MULMATRIXSTATE_H
#define MULMATRIXSTATE_H

#include <state.h>

class MulMatrixState : public State
{
public:
    MulMatrixState(State *parentState);

    const QString &getDescription() const
    {
        static const QString Desc = "Multiply matrixes";
        return Desc;
    }

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);
};

#endif // MULMATRIXSTATE_H
