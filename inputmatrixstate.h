#ifndef INPUTMATRIXSTATE_H
#define INPUTMATRIXSTATE_H

#include <state.h>

class InputMatrixState : public State
{
public:
    InputMatrixState(State *parentState, bool userInput);

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);

    const QString &getDescription() const
    {
        static const QString Desc = "Input matrix";
        return Desc;
    }

private:
    const bool mUserInput;
};

#endif // INPUTMATRIXSTATE_H
