#ifndef SHOWMATRIXSTATE_H
#define SHOWMATRIXSTATE_H

#include <state.h>

class ShowMatrixState : public State
{
public:
    ShowMatrixState(State *parentState);

    const QString &getDescription() const
    {
        static const QString Desc = "Display cached matrix";
        return Desc;
    }

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);
};

#endif // SHOWMATRIXSTATE_H
