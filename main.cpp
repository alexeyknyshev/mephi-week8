#include <QTextStream>
#include <applicationlogic.h>

#include <matrix.h>

int main()
{
    QTextStream in(stdin, QIODevice::ReadOnly);
    QTextStream out(stdout, QIODevice::WriteOnly);

    ApplicationLogic(in, out);

    /*
    Matrix a;
    a.setAt(0, 0, -5);
    a.setAt(1, 0, 1);
    a.setAt(2, 0, 2);
    a.setAt(0, 1, 7);
    a.setAt(1, 1, -1);
    a.setAt(2, 1, 3);

    out << "Matrix A[" << a.getRowCount() << ", " << a.getColumnCount() << "]" << "\n";
    out << a.toString() << "\n"
        << "\n*\n\n";
    out.flush();

    Matrix b;
    b.setAt(0, 0, 1);
    b.setAt(1, 0, 7);
    b.setAt(0, 1, -1);
    b.setAt(1, 1, -8);
    b.setAt(0, 2, 0);
    b.setAt(1, 2, 9);
    b.setAt(0, 3, 3);
    b.setAt(1, 3, 5);

    out << "Matrix B[" << b.getRowCount() << ", " << b.getColumnCount() << "]" << "\n";

    out << b.toString() << "\n";
    out.flush();

    Matrix c = a * b;


    out << "\n=\n\n"
        << c.toString()
        << "\n\n";


    Matrix d;
    d.setAt(0, 0, -1);
    d.setAt(1, 0, -2);
    d.setAt(0, 1, -3);
    d.setAt(1, 1, -4);

    out << "Matrix D[" << d.getRowCount() << ", " << d.getColumnCount() << "]" << "\n";

    out << d.toString() << "\n\n";
    out.flush();

    Matrix e;
    e.setAt(0, 0, 1);
    e.setAt(1, 0, 2);
    e.setAt(0, 1, 3);
    e.setAt(1, 1, 4);

    out << "Matrix E[" << e.getRowCount() << ", " << e.getColumnCount() << "]" << "\n";

    out << e.toString() << "\n\n";
    out.flush();

    out << "\n" << (d + e).toString() << "\n"
        << "\n" << (d - e).toString() << "\n\n";
    out.flush();
    */

//    Matrix a;
//    a.setAt(0, 0, 1);
//    a.setAt(0, 1, 2);
//    a.setAt(0, 2, 4);
//    a.setAt(1, 0, 2);
//    a.setAt(1, 1, 0);
//    a.setAt(1, 2, 3);

//    Matrix b;
//    b.setAt(0, 0, 2);
//    b.setAt(0, 1, 5);
//    b.setAt(1, 0, 1);
//    b.setAt(1, 1, 3);
//    b.setAt(2, 0, 1);
//    b.setAt(2, 1, 1);

//    Matrix c = a * b;
//    out << c.toString() << "\n";
//    out.flush();

    /*
    Matrix a;
    Matrix b;

    a.setAt(0, 0, 3);
    a.setAt(0, 1, 4);
    a.setAt(0, 2, 2);
    a.setAt(0, 3, 5);
    a.setAt(1, 0, 0);
    a.setAt(1, 1, -1);
    a.setAt(1, 2, 3);
    a.setAt(1, 3, 2);
    a.setAt(2, 0, 1);
    a.setAt(2, 1, 2);
    a.setAt(2, 2, 3);
    a.setAt(2, 3, 0);

    b.setAt(0, 0, 1);
    b.setAt(0, 1, 2);
    b.setAt(0, 2, 3);
    b.setAt(1, 0, -3);
    b.setAt(1, 1, 5);
    b.setAt(1, 2, 4);
    b.setAt(2, 0, 6);
    b.setAt(2, 1, 2);
    b.setAt(2, 2, 1);
    b.setAt(3, 0, 1);
    b.setAt(3, 1, -1);
    b.setAt(3, 2, 0);

    Matrix c = a * b;
    out << c.toString() << "\n";
    out.flush();
    */

    return 0;
}
