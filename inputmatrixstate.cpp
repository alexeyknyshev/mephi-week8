#include "inputmatrixstate.h"

#include <QTextStream>
#include <QStringList>
#include <applicationlogic.h>
#include <matrix.h>

InputMatrixState::InputMatrixState(State *parentState, bool userInput)
    : State(parentState),
      mUserInput(userInput)
{ }

State *InputMatrixState::process(QTextStream &input, QTextStream &output,
                                 ApplicationLogic *logic)
{
    if (mUserInput)
    {
        output << "Insert matrix cell values row by row and splitted by whitespaces." << "\n"
               << "Finally, to finish input press Return (Enter) twice." << "\n";
        output.flush();
    }

    QString line;

    Matrix mat;
    for (int row = 0; !(line = input.readLine()).isEmpty(); ++row)
    {
        QStringList spiltedLine =
                line.split(QRegExp("\\s+"), QString::SkipEmptyParts);

        for (int index = 0, col = 0; index < spiltedLine.size(); ++index)
        {
            bool ok = false;
            const float val = spiltedLine.at(index).toFloat(&ok);

            if (ok)
            {
                mat.setAt(row, col, val);
                ++col;
            }
        }
    }

    if (!mat.isNull())
    {
        int id = logic->addMatrix(mat);
        output << "Inputed matrix id \"" << QString::number(id) << "\"." << "\n";
        output.flush();
    }

    return mParentState;
}
