#ifndef STATE_H
#define STATE_H

#include <QList>
#include <QString>

class QTextStream;
class ApplicationLogic;

class State
{
public:
    State(State *parentState)
        : mParentState(parentState)
    { }

    virtual ~State();

    virtual State *process(QTextStream &input, QTextStream &output,
                           ApplicationLogic *logic);

    virtual const QString &getDescription() const = 0;

    void operator +=(State *state);

protected:
    State *mParentState;
    QList<State *> mChildStates;
};

#endif // STATE_H
