#ifndef MATRIX_H
#define MATRIX_H

#include <QVector>

class Matrix
{
public:
    Matrix();

    float getAt(int row, int column) const;
    void setAt(int row, int column, float scalar);

    int getRowCount() const
    {
        return mData.size();
    }

    int getColumnCount() const
    {
        if (!mData.isEmpty())
        {
            return mData.at(0).size();
        }

        return 0;
    }

    Matrix operator+(const Matrix &other) const;
    Matrix operator-(const Matrix &other) const;
    Matrix operator*(const Matrix &other) const;

    bool isOkForAddSubWith(const Matrix &other) const;
    bool isOkForMulWith(const Matrix &other) const;

    void setNull()
    {
        mData.clear();
    }

    bool isNull() const
    {
        return mData.empty();
    }

    QString toString() const;

    static const Matrix null;

private:
    QVector<QVector<float>> mData;
};

#endif // MATRIX_H
