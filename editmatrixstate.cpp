#include "editmatrixstate.h"

#include <applicationlogic.h>

#include <QTextStream>

EditMatrixState::EditMatrixState(State *parentState)
    : State(parentState)
{ }

State *EditMatrixState::process(QTextStream &input, QTextStream &output,
                                ApplicationLogic *logic)
{
    const int count = logic->getMatrixCount();
    if (count >= 1)
    {
        output << "Select matrix to edit by id (0 - " + QString::number(count - 1) << ")\n";
        output.flush();
        QString line = input.readLine();
        bool ok = false;
        int id = line.toInt(&ok);
        if (!ok)
        {
            output << "Abort." << "\n";
            return mParentState;
        }

        output << "\n";
        output.flush();

        Matrix &mat = logic->getMatrix(id);

        if (id >= 0 && id < count)
        {
            if (mat.isNull())
            {
                output << "Selected matrix is NULL! Please select another one." << "\n";
                output.flush();
                return process(input, output, logic);
            }
            else
            {
                const int rowCount = mat.getRowCount();
                const int colCount = mat.getColumnCount();

                bool ok = false;

                int row = 0;
                while (!ok)
                {
                    output << "Select matrix row to edit (0 - " << QString::number(rowCount - 1) << ")." << "\n";
                    output.flush();
                    input >> row;
                    if (row >= 0 && row < rowCount)
                    {
                        ok = true;
                    }
                    else
                    {
                        output << "Selected row \"" + QString::number(row) + "\" is out of range!";
                        output.flush();
                    }
                }

                ok = false;

                int col;
                while (!ok)
                {
                    output << "Select matrix column to edit (0 - " << QString::number(colCount - 1) << ")." << "\n";
                    output.flush();
                    input >> col;
                    if (col >= 0 && col < colCount)
                    {
                        ok = true;
                    }
                    else
                    {
                        output << "Selected column \"" + QString::number(col) + "\" is out of range!";
                        output.flush();
                    }
                }

                output << "Selected index (" << QString::number(row) << "; " << QString::number(col) << ") equals " <<
                          QString::number(mat.getAt(row, col)) << "." << "\n" <<
                          "Input new value for this cell" << "\n";
                output.flush();

                float newVal;
                input >> newVal;

                mat.setAt(row, col, newVal);
            }
        }
        else
        {
            return this;
        }
    }
    else
    {
        output << "No matrix has been input!" << "\n";
    }

    output << "\n";
    output.flush();

    return mParentState;
}
