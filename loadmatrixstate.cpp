#include "loadmatrixstate.h"

#include <QTextStream>
#include <QFile>
#include <QFileInfo>

#include <inputmatrixstate.h>

LoadMatrixState::LoadMatrixState(State *parentState)
    : State(parentState)
{ }

State *LoadMatrixState::process(QTextStream &input, QTextStream &output,
                                ApplicationLogic *logic)
{
    output << "Select matrix file." << "\n";
    output.flush();
    QString fileName = input.readLine();
    if (fileName.isEmpty())
    {
        return mParentState;
    }

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream fileStream(&file);
        InputMatrixState input(nullptr, false);
        input.process(fileStream, output, logic);
    }
    else
    {
        QFileInfo info(file);
        if (!info.exists())
        {
            output << "File " + fileName + " doesn't exist!" << "\n";
        }
        else
        {
            output << "Cannot open file " + fileName + " for reading!" << "\n";
        }

        output.flush();
        return this;
    }

    return mParentState;
}
