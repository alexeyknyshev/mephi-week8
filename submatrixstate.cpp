#include "submatrixstate.h"

#include <applicationlogic.h>

#include <QTextStream>

SubMatrixState::SubMatrixState(State *parentState)
    : State(parentState)
{ }

State *SubMatrixState::process(QTextStream &input, QTextStream &output, ApplicationLogic *logic)
{
    const int count = logic->getMatrixCount();
    if (count >= 2)
    {
        output << "Select 2 matrixes by id (0 - " + QString::number(count - 1) << ")\n";
        output.flush();
        int id1, id2;
        input >> id1 >> id2;
        output << "\n";
        output.flush();
        if (id1 >= 0 && id1 < count &&
            id2 >= 0 && id2 < count)
        {
            const Matrix mat1 = logic->getMatrix(id1);
            const Matrix mat2 = logic->getMatrix(id2);

            if (mat1.isOkForAddSubWith(mat2))
            {
                output << mat1.toString() << "\n";
                output << "\t-\n";
                output << mat2.toString() << "\n";

                Matrix result = mat1 - mat2;

                output << "\t=\n";
                output << result.toString() << "\n";

                output << "Result matrix id \"" << logic->addMatrix(result) << "\"." << "\n";
                output.flush();
            }
            else
            {
                output << "Matrix:\n" << mat2.toString() << "\n";
                output << "cannot be sub from matrix:\n" << mat1.toString() << "\n";
                output << "(Size of first doesn\'t equal size of second)\n";
            }
        }
        else
        {
            return this;
        }
    }
    else
    {
        output << "Two matrixes required for substract operation!" << "\n";
    }

    output << "\n";
    output.flush();

    return mParentState;
}
