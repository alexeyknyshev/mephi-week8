#ifndef LOADMATRIXSTATE_H
#define LOADMATRIXSTATE_H

#include <state.h>

class LoadMatrixState : public State
{
public:
    LoadMatrixState(State *parentState);

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);

    const QString &getDescription() const
    {
        static const QString Desc = "Load matrix from file";
        return Desc;
    }
};

#endif // LOADMATRIXSTATE_H
