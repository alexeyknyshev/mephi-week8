#ifndef EXITSTATE_H
#define EXITSTATE_H

#include <state.h>

#include <QObject>

class ExitState : public State
{
public:
    ExitState(State *parentState);

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);

    const QString &getDescription() const
    {
        static const QString Descr = "Exit";
        return Descr;
    }
};

#endif // EXITSTATE_H
