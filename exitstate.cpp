#include "exitstate.h"

ExitState::ExitState(State *parentState)
    : State(parentState)
{ }

State *ExitState::process(QTextStream &input, QTextStream &output,
                          ApplicationLogic *logic)
{
    Q_UNUSED(input);
    Q_UNUSED(output);
    Q_UNUSED(logic);
    return nullptr;
}
