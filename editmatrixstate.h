#ifndef EDITMATRIXSTATE_H
#define EDITMATRIXSTATE_H

#include <state.h>

class EditMatrixState : public State
{
public:
    EditMatrixState(State *parentState);

    const QString &getDescription() const
    {
        static const QString Desc = "Edit cached matrix";
        return Desc;
    }

    State *process(QTextStream &input, QTextStream &output,
                   ApplicationLogic *logic);
};

#endif // EDITMATRIXSTATE_H
