#include "savematrixstate.h"

#include <QFile>
#include <QTextStream>

#include <applicationlogic.h>

SaveMatrixState::SaveMatrixState(State *parentState)
    : State(parentState)
{ }

State *SaveMatrixState::process(QTextStream &input, QTextStream &output, ApplicationLogic *logic)
{
    const int count = logic->getMatrixCount();
    if (count >= 1)
    {
        output << "Select matrix to save by id (0 - " << QString::number(count - 1) << ").\n";
        output.flush();

        int id;
        input >> id;
        input.readLine();

        if (id < 0 || id >= count)
        {
            output << "Matrix index out of range. Try again." << "\n";
            return this;
        }

        output << "Select output file path." << "\n";
        output.flush();

        QString fileName = input.readLine();
        if (fileName.isEmpty())
        {
            output << "Abort." << "\n";
            return mParentState;
        }

        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        {
            QTextStream fileStream(&file);

            fileStream << logic->getMatrix(id).toString() << "\n";
            fileStream.flush();

            if (fileStream.status() == QTextStream::Ok)
            {
                output << "Matrix file successfully written!" << "\n";
            }
            else if (fileStream.status() == QTextStream::WriteFailed)
            {
                output << "Matrix file write failed!" << "\n";
            }

            return mParentState;
        }
        else
        {
            output << "Cannot open file " + fileName + " for writing!" << "\n";
            output.flush();
            return this;
        }
    }
    else
    {
        output << "There is not any cached matrix yet. Please, input matrix or load it from file." << "\n";
    }

    return mParentState;
}
