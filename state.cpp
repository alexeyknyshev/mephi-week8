#include "state.h"

#include <QTextStream>

State::~State()
{
    mParentState = nullptr;
    foreach (State *state, mChildStates)
    {
        delete state;
    }
    mChildStates.clear();
}

State *State::process(QTextStream &input, QTextStream &output,
                      ApplicationLogic *logic)
{
    Q_UNUSED(logic);

    for (int i = 0; i < mChildStates.size(); ++i)
    {
        output << QString::number(i + 1) << ". "
               << mChildStates.at(i)->getDescription() << "\n";
    }
    output.flush();

    int res = 0;
    input >> res;
    input.readLine();

    if (res >= 1 && res <= mChildStates.size())
    {
        return mChildStates.at(res - 1);
    }

    return this;
}

void State::operator +=(State *state)
{
    mChildStates += state;
}
