#include "showmatrixstate.h"

#include <QTextStream>

#include <applicationlogic.h>

ShowMatrixState::ShowMatrixState(State *parentState)
    : State(parentState)
{ }

State *ShowMatrixState::process(QTextStream &input, QTextStream &output,
                                ApplicationLogic *logic)
{
    const int count = logic->getMatrixCount();
    if (count >= 1)
    {
        output << "Select matrix by id (0 - " + QString::number(count - 1) << ")\n";
        output.flush();
        int id;
        input >> id;
        output << "\n";
        output.flush();
        if (id >= 0 && id < count)
        {
            output << logic->getMatrix(id).toString() << "\n";
        }
        else
        {
            return this;
        }
    }
    else
    {
        output << "No matrix has been input!" << "\n";
    }

    output << "\n";
    output.flush();

    return mParentState;
}
