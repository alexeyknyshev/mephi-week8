#-------------------------------------------------
#
# Project created by QtCreator 2013-09-14T22:10:14
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = week8
CONFIG   += console
CONFIG   -= app_bundle

QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app


SOURCES += main.cpp \
    applicationlogic.cpp \
    rootstate.cpp \
    state.cpp \
    exitstate.cpp \
    inputmatrixstate.cpp \
    matrix.cpp \
    showmatrixstate.cpp \
    addmatrixstate.cpp \
    submatrixstate.cpp \
    mulmatrixstate.cpp \
    editmatrixstate.cpp \
    loadmatrixstate.cpp \
    savematrixstate.cpp

HEADERS += \
    applicationlogic.h \
    rootstate.h \
    state.h \
    exitstate.h \
    inputmatrixstate.h \
    matrix.h \
    showmatrixstate.h \
    addmatrixstate.h \
    submatrixstate.h \
    mulmatrixstate.h \
    editmatrixstate.h \
    loadmatrixstate.h \
    savematrixstate.h
